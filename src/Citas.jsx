import React, { useEffect, useState } from 'react';
import styled from "styled-components";

const Foto = styled.div`
    width: 400px;
    height: 400px;
    border: 3px solid black;
    border-radius: 20px;
    background-size: cover;
    overflow: hidden;
`
const Cita = styled.div`
    margin: 150px 50px 50px 50px;
   
`

const Img = styled.img`
    width: 100%;
    min-height: 100%
`
const Titulo = styled.h1`
    margin: 20px
`

const Citas = () => {

    const [datos, setDatos] = useState([]);

    const url = 'https://edwin-citas-app.herokuapp.com/citas';

    const cargaCitas = () => {
        fetch(url)
            .then(resp => resp.json())
            .then(datos => setDatos(datos))
            .catch(err => console.log("Se ha producido un error", err))
    }

    useEffect(() => {
        cargaCitas();
    }, []);


    return (
        <>
            <Titulo>Citas Celebres</Titulo>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-1"></div>
                    <div className="sm-12 order-sm-2 lg-5 order-lg-1">

                        <Foto><Img src={datos.img} alt="" /></Foto>

                    </div>
                    <div className="sm-12 order-sm-1 lg-6 order-lg-2">
                        <Cita>
                            <h4>{datos.cita}</h4>

                            <p className="text-right">-{datos.autor}-</p>
                        </Cita>

                    </div>

                </div>
            </div>

        </>
    )
}

export default Citas;