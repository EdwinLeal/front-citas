import Citas from './Citas';
import './App.css';

function App() {
  return (
    <div className="App">
      <Citas />
    </div>
  );
}

export default App;
